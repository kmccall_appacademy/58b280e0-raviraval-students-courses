class Student
  attr_accessor :first_name, :last_name

  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    @courses << course unless @courses.include?(course)
    course.students << self
    if self.has_conflict?
      raise "Courses conflict!"
    end
  end

  def course_load
    dep_credits = Hash.new(0)
    @courses.each do |course|
      dep_credits[course.department] += course.credits
    end
    dep_credits
  end

  def has_conflict?
    @courses.each_index do |idx|
      @courses.each_index do |jdx|
        unless idx == jdx
          if @courses[idx].conflicts_with?(@courses[jdx])
            return true
          end
        end
      end
    end
    false
  end

end
